module ContextualPNs
using ..context

export CompiledPetriNet, PetriNet, Place, Transition, NormalArc, InhibitorArc, TestArc, compile, on, off, Update

abstract type PNObject end

mutable struct Place <: PNObject 
    const name::String
    token::Real
end 

@enum UpdateValue on off

mutable struct Update
    context::Context
    updateValue::UpdateValue
end 

struct Transition <: PNObject 
    name::String
    contexts::Union{<:Context, Any, <:context.AbstractContextRule}
    updates::AbstractArray{Update}
end

abstract type Arc end

Base.@kwdef mutable struct NormalArc <: Arc
    const from::PNObject
    const to::PNObject
    weight::Real
    priority::Int = 0
end
mutable struct InhibitorArc <: Arc
    const from::Place
    const to::Transition
    weight::Real
end
mutable struct TestArc <: Arc
    const from::Place
    const to::Transition
    weight::Real
end

Base.@kwdef mutable struct PetriNet
    places::Vector{Union{Any, Place}} = []
    transitions::Vector{Union{Any, Transition}} = []
    arcs::Vector{Union{Any, <:Arc}} = []
end

mutable struct CompiledPetriNet
    WeightMatrix_in::Matrix
    WeightMatrix_out::Matrix
    WeightMatrix_inhibitor::Matrix
    WeightMatrix_test::Matrix
    tokenVector::Vector
    PrioritiesMatrix::Matrix
    ContextMatrices::Vector
    UpdateMatrix::Matrix
    ContextMap::Dict
end

function genContextRuleMatrix(cr::T, cdict::Dict, nc::Int) where {T <: Union{Context, Any, context.AbstractContextRule}}
    matrix = zeros(1, nc)
    if typeof(cr) <: context.AbstractContextRule
        if cr isa AndContextRule
            a = genContextRuleMatrix(cr.c1, cdict, nc)
            b = genContextRuleMatrix(cr.c2, cdict, nc)
            matrix = nothing
            c = 0
            for i in 1:size(a)[1]
                for j in 1:size(b)[1]
                    findmin((a[i, :] .- b[j, :]) .* b[j, :])[1] < -1 ? c = zeros(1, size(a)[2]) : c = a[i, :] .+ b[j, :]
                    c = reshape(c, 1, length(c))
                    matrix == nothing ? matrix = [c;] : matrix = [matrix; c]
                end            
            end       
        elseif cr isa OrContextRule
            matrix = [genContextRuleMatrix(cr.c1, cdict, nc); genContextRuleMatrix(cr.c2, cdict, nc)]
        else
            matrix = -genContextRuleMatrix(cr.c, cdict, nc)
        end
    elseif typeof(cr) <: Context
        matrix[cdict[cr]] = 1
    end
    matrix
end

function compile(pn::PetriNet)
    # should test here if name is given two times
    # should test here if arcs are connected correctly (not place to place etc.)
    np = length(pn.places)                              # number of places
    nt = length(pn.transitions)                         # number of transitions
    nc = length(getContexts())                          # number of contexts
    W_i = zeros(Float64, np, nt)                        # Input Arc weights matrix (to place)
    W_o = zeros(Float64, np, nt)                        # Output Arc weights matrix(from place)
    W_inhibitor = zeros(Float64, np, nt) .+ Inf         # Inhibitor Arc weights matrix
    W_test = zeros(Float64, np, nt)                     # Test Arc weights matrix
    t = zeros(Float64, np)                              # Token vector
    P = zeros(Float64, np, nt)                          # Priority matrix
    pdict = Dict()                                      # dictionary of places and corresponding index
    tdict = Dict()                                      # dictionary of transitions and corresponding index
    cdict = Dict()                                      # dictionary of contexts and corresponding index

    for (i, place) in enumerate(pn.places)
        t[i] = place.token
        pdict[place.name] = i
    end
    for (i, transition) in enumerate(pn.transitions)
        tdict[transition.name] = i
    end
    for (i, context) in enumerate(getContexts())
        cdict[context] = i
    end


    C = nothing                                         # Context matrix
    U = zeros(Float64, nc, nt)                          # Update matrix
    for transition in pn.transitions
        c = sign.(genContextRuleMatrix(transition.contexts, cdict, nc))
        C == nothing ? C = [c] : C = [C; [c]]
        for update in transition.updates
            if update.updateValue == on
                U[cdict[update.context], tdict[transition.name]] = 1
            else
                U[cdict[update.context], tdict[transition.name]] = -1
            end
        end 
    end
    for arc in pn.arcs
        if arc.from isa Place
            if arc isa NormalArc
                W_o[pdict[arc.from.name], tdict[arc.to.name]] = arc.weight
                if !(arc.priority in P[pdict[arc.from.name]])
                    P[pdict[arc.from.name], tdict[arc.to.name]] = arc.priority
                else
                    print("check priority of place ", arc.from.name)
                end
            elseif arc isa InhibitorArc
                W_inhibitor[pdict[arc.from.name], tdict[arc.to.name]] = arc.weight
            else
                W_test[pdict[arc.from.name], tdict[arc.to.name]] = arc.weight
            end
        else
            W_i[pdict[arc.to.name], tdict[arc.from.name]] = arc.weight
        end
    end
    CompiledPetriNet(W_i, W_o, W_inhibitor, W_test, t, P, C, U, cdict)
end

end