module context

export addContext, getActiveContexts, isActive, activateContext, deactivateContext, addMixin, getContexts, getMixins, getMixin, @newContext, @newMixin, @context, assignMixin, disassignMixin, Context, Mixin, AndContextRule, OrContextRule, NotContextRule

using Parameters

abstract type Mixin end

abstract type Context end

@with_kw mutable struct ContextManagement
	contexts::Vector{Context} = []
	activeContexts::Vector{Context} = []
	mixins::Dict{Context, Dict{Any, Vector{DataType}}} = Dict()
	mixinTypeDB::Dict{Any, Dict{Context, DataType}} = Dict()
	mixinDB::Dict{Any, Dict{Context, Any}} = Dict()
end

global contextManager = ContextManagement()

function getContexts()
	contextManager.contexts
end

function addContext(context::T) where {T <: Context}
	push!(contextManager.contexts, eval(context))
	push!(contextManager.activeContexts, eval(context))
end

function getActiveContexts()
	contextManager.activeContexts
end

function activateContext(context::T) where {T <: Context}
	if !(context in contextManager.activeContexts) push!(contextManager.activeContexts, eval(context)) end
end

function deactivateContext(context::T) where {T <: Context}
	deleteat!(contextManager.activeContexts, findall(x->x==context, contextManager.activeContexts))
end

function isActive(context::T) where {T <: Context}
	context in contextManager.activeContexts
end

function addMixin(context, contextualType, mixinNameSymbol)
	if (context) in keys(contextManager.mixins)
		if (contextualType) in keys(contextManager.mixins[context])
			push!(contextManager.mixins[context][contextualType], mixinNameSymbol)
		else
			contextManager.mixins[context][contextualType] = [mixinNameSymbol]
		end
	else
		contextManager.mixins[context] = Dict((contextualType)=>[mixinNameSymbol])
	end
end

function getMixins()
	contextManager.mixins
end

function getMixins(type)
	contextManager.mixinDB[type]
end

function getMixin(type, context::T) where {T <: Context}
	(contextManager.mixinDB[type])[context]
end

macro newContext(contextName)
	if typeof(contextName) == String || typeof(contextName) == Symbol
		if typeof(contextName) == String
			contextName = Meta.parse(contextName)
		end
		contextTypeNameSymbol = Symbol(contextName, :ContextType)

		structDefExpr = :(struct $(contextTypeNameSymbol) <: Context end;)
		SingletonDefExpr = :($(contextName) = $contextTypeNameSymbol())

		return esc(:($structDefExpr; $SingletonDefExpr; addContext($contextName)))
	else
		error("Argument of @newContext must be a String or a Symbol")
	end
end


macro newMixin(mixin, attributes, context)
	contextualType = Symbol(strip((split(repr(mixin), "<:")[2])[1:end-1]))
	mixin = Symbol(strip((split(repr(mixin), " <: ")[1])[3:end]))
	Base.remove_linenums!(attributes)

	newStructExpr = :(struct $mixin <: Mixin
		$attributes
	end)

	return esc(:($newStructExpr; addMixin($context, $contextualType, $mixin)))
end

function assignMixin(pair::Pair, context::T) where {T<:Context}
	type = pair[1]
	mixin = pair[2]
	if type in keys(contextManager.mixinDB)
		if context in keys(contextManager.mixinDB[type])
			@warn(repr(type)*" already has Mixin in context "*repr(context)*". Previous Mixin will be overwritten!")
		end
		contextManager.mixinDB[type][context] = mixin
	else
		contextManager.mixinDB[type] = Dict(context => mixin)
	end
	if type in keys(contextManager.mixinTypeDB)
		contextManager.mixinTypeDB[type][context] = typeof(mixin)
	else
		contextManager.mixinTypeDB[type] = Dict(context => typeof(mixin))
	end
end

function disassignMixin(pair::Pair, context::T) where {T<:Context}
	type = pair[1]
	mixin = pair[2]
	if type in keys(contextManager.mixinDB)
		delete!(contextManager.mixinDB[type], context)
	else
		error("Mixin is not assigned to type "*repr(type))
	end
	if type in keys(contextManager.mixinTypeDB)
		delete!(contextManager.mixinTypeDB[type], context)
	else
		error("Mixin is not assigned to type "*repr(type))
	end
end

macro context(cname, expr)
	if typeof(expr) != Expr
		error("Second argument of @context must be a function or function definition")
	else
		if expr.head == :function
			functionHeaderString = repr(expr.args[1])
			if endswith(repr(expr.args[1]), "())")
				functionHeaderString = functionHeaderString[1:end-2] * "context::" * repr(cname)[2:end] * "ContextType))"
			else
				functionHeaderString = functionHeaderString[1:end-2] * ", " * "context::" * repr(cname)[2:end] * "ContextType))"
			end
			expr.args[1] = eval(Meta.parse(functionHeaderString))
			return esc(expr)
		elseif expr.head == :call
			callString = repr(expr)
			contextString = repr(cname)
			if endswith(callString, "())")
				callString = callString[1:end-2] *  contextString[2:end] * "))"
			else
				callString = callString[1:end-2] * ", " * contextString[2:end] * "))"
			end
			callExpr = Meta.parse(callString)
			return esc(eval(callExpr))
		elseif expr.head == :macrocall
			callString = repr(expr)
			contextString = repr(cname)
			callString = callString[3:end-1] * " " * contextString[2:end] 
			callExpr = Meta.parse(callString)
			return esc(callExpr)
		else
			error("Second argument of @context must be a function or function definition")
		end
	end
end


abstract type AbstractContextRule end

struct AndContextRule <: AbstractContextRule
	c1::Union{<:Context, <:AbstractContextRule}
	c2::Union{<:Context, <:AbstractContextRule}
end

struct OrContextRule <: AbstractContextRule
	c1::Union{<:Context, <:AbstractContextRule}
	c2::Union{<:Context, <:AbstractContextRule}
end

struct NotContextRule <: AbstractContextRule
	c::Union{<:Context, <:AbstractContextRule}
end

function isActive(contextRule::T) where {T <: AbstractContextRule}
	if contextRule isa AndContextRule
		isActive(contextRule.c1) & isActive(contextRule.c2)
	elseif contextRule isa OrContextRule
		isActive(contextRule.c1) | isActive(contextRule.c2)
	else
		!isActive(contextRule.c)
	end
end

function getContextsOfRule(contextRule::T) where {T <: AbstractContextRule}
	contexts = []
	if ((contextRule isa AndContextRule) | (contextRule isa OrContextRule))
		if typeof(contextRule.c1) <: AbstractContextRule
			append!(contexts, getContextsOfRule(contextRule.c1))
		else
			append!(contexts, [contextRule.c1])
		end
		if typeof(contextRule.c2) <: AbstractContextRule
			append!(contexts, getContextsOfRule(contextRule.c2))
		else
			append!(contexts, [contextRule.c2])
		end
	else
		if typeof(contextRule.c) <: AbstractContextRule
			append!(contexts, getContextsOfRule(contextRule.c))
		else
			append!(contexts, [contextRule.c])
		end
	end
	union(contexts)
end

function isActive(context::Nothing)
	true
end

function Base.:&(c1::CT1, c2::CT2) where {CT1, CT2 <: Union{Context, AbstractContextRule}}
    AndContextRule(c1, c2)
end

function Base.:|(c1::CT1, c2::CT2) where {CT1, CT2 <: Union{Context, AbstractContextRule}}
    OrContextRule(c1, c2)
end

function Base.:!(c::CT) where {CT <: Union{Context, AbstractContextRule}}
    NotContextRule(c)
end

end